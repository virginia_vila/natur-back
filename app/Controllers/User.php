<?php

namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class User extends BaseController
{
    /**
     * Lista con todos los usuarios
     * @return Response
     */

    public function index()
    {
        $model = new UserModel();
        return $this->getResponse( $model->findAll()   
        );
    }


    public function store()
    {
        $rules = [
            'nombre' => 'required',
            'email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[client.email]',
            'retainer_fee' => 'required|max_length[255]'
        ];

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $rules)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $userEmail = $input['email'];

        $model = new ClientModel();
        $model->save($input);

        $user = $model->where('email', $userEmail)->first();

        return $this->getResponse(
            [
                'message' => 'Usuario añadido correctamente',
                'user' => $user
            ]
        );
    }


    public function show($id = null)
    {
        try {

            $model = new UserModel();
            $user = $model->findUserById($id);

            return $this->getResponse(
                [
                    'message' => 'Usuario mostrado correctamente',
                    'user' => $user
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'No podemos encontrar el usuario con el id indicado'
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    public function showByEmail($email = null)
    {
        try {

            $model = new UserModel();
            $user = $model->findUserByEmailAddress($email);

            return $this->getResponse($user);

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'No podemos encontrar el usuario con el email indicado'
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }





    public function update($id)
    {
        try {

            $model = new UserModel();
            $model->findUserById($id);

            $input = $this->getRequestInput($this->request);

            $model->update($id, $input);
            $user = $model->findUserById($id);

            return $this->getResponse(
                [
                    'message' => 'Usuario cambiado correctamente',
                    'usuario' => $user 
                ]
            );

        } catch (Exception $exception) {

            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    public function destroy($id)
    {
        try {

            $model = new UserModel();
            $user = $model->findUserById($id);
            $model->delete($user);

            return $this
                ->getResponse(
                    [
                        'message' => 'Usuario borrado correctamente',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }
}