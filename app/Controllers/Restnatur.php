<?php 

namespace App\Controllers;
use App\Models\NaturModel;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\Filters\FilterInterface;

class RESTNatur extends ResourceController {
	

	protected $modelName = 'App\Models\NaturModel';
 	protected $format = 'json';
    protected $producto = '';

	private function genericResponse($data, $msj, $code){
		if ($code == 200) {
			return $this->respond($data);
		} else {
			return $this->respond(array(
			"msj" => $msj,
			"code" => $code
			));
		}
	}
	
	private function url($segmento){
		if(isset($_SERVER['HTTPS'])){
			$protocol = ($_SERVER['HTTPS'] != "off") ? "https" : "http";
 		} else{
			$protocol = 'http';
			
		}
 		return $protocol . "://" . $_SERVER['HTTP_HOST'] . $segmento;
 	}
 	
 	private function map($data){

 		$productos = array();
 		foreach ($data as $row){
			$producto = array(
 				"id" => $row['id'],
 				"nombre" => $row['nombre'],
				"descripcion" => $row['descripcion'],
				"imagen" => $row['imagen'],
				"tipo_producto" => $row['tipo_producto'],
				"url" => $this->url("/restnatur/".$row['id']),
				"id_categ" => $row['id_categ'],
				"nombre_categ" => $row['nombre_categ']

 			);
 			array_push($productos, $producto);
 		}
 		return $productos;
 	}
 	
 	 public function index(){
	     header('Access-Control-Allow-Origin: *');
		 $data = $this->model->getAll();
		 $productos = $this->map($data);
		 return $this->genericResponse($productos, null, 200);
	}
	
	public function show($id = null){
		header('Access-Control-Allow-Origin: *');
		$data = $this->model->get($id);
		$producto = $this->map($data);
		return $this->genericResponse($producto, null, 200);
	}


    public function delete($id = null){
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: DELETE");
		$data = $this->model->get($id);
		$producto = $this->map($data);
		$this->model->delete($id);
		return $this->genericResponse($producto, null, 200);
	}

	public function create(){
		header('content-type: application/json');
			$producto= new NaturModel();
				$id = $producto->insert([
				'nombre' => $this->request->getJsonVar('nombre'),
				'imagen' => $this->request->getJsonVar('imagen'),
				'descripcion' => $this->request->getJsonVar('descripcion'),
				'tipo_producto' => $this->request->getJsonVar('tipo_producto'),
				'id_categ' => $this->request->getJsonVar('id_categ'),
				]); 
				return $this->genericResponse($this->model->find($id), null,
				200);
			}

	public function update($id = null){
			header('content-type: application/json');
				$producto= new NaturModel();
				 $producto->update($id,[
					'nombre' => $this->request->getJsonVar('nombre'),
					'imagen' => $this->request->getJsonVar('imagen'),
					'descripcion' => $this->request->getJsonVar('descripcion'),
					'tipo_producto' => $this->request->getJsonVar('tipo_producto'),
					'id_categ' => $this->request->getJsonVar('id_categ'),
						]); 
						return $this->genericResponse($this->model->find($id), null,
						200);
	}
	
	
}
