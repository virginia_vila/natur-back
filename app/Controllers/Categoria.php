<?php 

namespace App\Controllers;
use App\Models\CategoriaModel;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\Filters\FilterInterface;

class Categoria extends ResourceController {
	

	protected $modelName = 'App\Models\CategoriaModel';
 	protected $format = 'json';
    protected $categoria = '';

	private function genericResponse($data, $msj, $code){
		if ($code == 200) {
			return $this->respond($data);
		} else {
			return $this->respond(array(
			"msj" => $msj,
			"code" => $code
			));
		}
	}
	
	private function url($segmento){
		if(isset($_SERVER['HTTPS'])){
			$protocol = ($_SERVER['HTTPS'] != "off") ? "https" : "http";
 		} else{
			$protocol = 'http';
			
		}
 		return $protocol . "://" . $_SERVER['HTTP_HOST'] . $segmento;
 	}
 	
 	private function map($data){

 		$categorias = array();
 		foreach ($data as $row){
			$categoria = array(
 				"id_categoria" => $row['id_categoria'],
 				"nombre" => $row['nombre'],
				"descripcion" => $row['descripcion'],
 			);
 			array_push($categorias, $categoria);
 		}
 		return $categorias;
 	}
 	
 	 public function index(){
		 $data = $this->model->getAll();
		 $categorias = $this->map($data);
		 return $this->genericResponse($categorias, null, 200);
	}
	
	public function show($id = null){
		$data = $this->model->get($id);
		$categoria = $this->map($data);
		return $this->genericResponse($categoria, null, 200);
	}


    public function delete($id = null){
		$data = $this->model->get($id);
		$categoria = $this->map($data);
		$this->model->delete($id);
		return $this->genericResponse($categoria, null, 200);
	}

	public function create(){

			$categoria= new CategoriaModel();
				$id = $categoria->insert([
				'nombre' => $this->request->getJsonVar('nombre'),
				'descripcion' => $this->request->getJsonVar('descripcion'),
				]); 
				return $this->genericResponse($this->model->find($id), null,
				200);
			}

	public function update($id = null){

				$categoria= new CategoriaModel();
				 $categoria->update($id,[
					'nombre' => $this->request->getJsonVar('nombre'),
					'descripcion' => $this->request->getJsonVar('descripcion'),
						]); 
						return $this->genericResponse($this->model->find($id), null,
						200);
	}
	
	
}
