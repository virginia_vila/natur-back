<?php 

namespace App\Controllers;
use App\Models\FavoritosModel;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\Filters\FilterInterface;

class Favoritos extends ResourceController {
	

	protected $modelName = 'App\Models\FavoritosModel';
 	protected $format = 'json';
    protected $favorito = '';

	private function genericResponse($data, $msj, $code){
		if ($code == 200) {
			return $this->respond($data);
		} else {
			return $this->respond(array(
			"msj" => $msj,
			"code" => $code
			));
		}
	}
	
	private function url($segmento){
		if(isset($_SERVER['HTTPS'])){
			$protocol = ($_SERVER['HTTPS'] != "off") ? "https" : "http";
 		} else{
			$protocol = 'http';
			
		}
 		return $protocol . "://" . $_SERVER['HTTP_HOST'] . $segmento;
 	}
 	
 	private function map($data){

 		$favoritos = array();
 		foreach ($data as $row){
			$favorito = array(
 				"id" => $row['id'],
 				"userid" => $row['userid'],
				"productoid" => $row['productoid'],
				"nombreproducto" => $row['nombreproducto'],
				"imagenproducto" => $row['imagenproducto'],
 			);
 			array_push($favoritos, $favorito);
 		}
 		return $favoritos;
 	}
 	
 	 public function index(){
		 $data = $this->model->getAll();
		 $favoritos = $this->map($data);
		 return $this->genericResponse($favoritos, null, 200);
	}
	
	public function show($id = null){
		$data = $this->model->get($id);
		$favorito = $this->map($data);
		return $this->genericResponse($favorito, null, 200);
	}


    public function delete($id = null){
		$data = $this->model->get($id);
		$favorito = $this->map($data);
		$this->model->delete($id);
		return $this->genericResponse($favorito, null, 200);
	}

	public function create(){
			$favorito= new FavoritosModel();
				$id = $favorito->insert([
				'userid' => $this->request->getJsonVar('userid'),
				'productoid' => $this->request->getJsonVar('productoid'),
				]); 
				return $this->genericResponse($this->model->find($id), null,
				200);
			}
	
	
}
