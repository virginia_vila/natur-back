<?php
namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use ReflectionException;

class Auth extends BaseController
{
    /**
     * Registro de nuevo usuario
     * @return Response
     * @throws ReflectionException
     */
    public function register()
    {
        $rules = [
            'nombre' => 'required',
            'email' => 'required|valid_email|is_unique[user.email]',
            'password' => 'required|min_length[8]|max_length[255]'
            
        ];

        $errors = [
            'nombre' => [
                'required' => 'Introduzca un nombre',           

            ],
            'password' => [
                'validateUser' => 'No encontramos este registro. Inténtelo de nuevo.',
                'min_length' => 'La contraseña tiene que tener al menos 8 carácteres',
                'required' => 'Introduzca la contraseña.',

            ],
            'email' => [
                'required' => 'Introduzca un email.',
                'valid_email' => 'El email no es válido',      
                'is_unique' => 'El email ya está en uso',        
            ]
        ];

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $rules, $errors)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $userModel = new UserModel();        
        $userModel->save($input);



        return $this
            ->getJWTForUser(
                $input['email'],
                ResponseInterface::HTTP_CREATED
            );
    }

    /**
     * Autentificación de usuario
     * @return Response
     */
    public function login(){
        $rules = [
            'email' => 'required|min_length[6]|max_length[50]|valid_email',
            'password' => 'required|min_length[8]|max_length[255]|validateUser[email, password]'
        ];
        $errors = [
            'password' => [
                'validateUser' => 'Credenciales incorrectas.',
                'min_length' => 'La contraseña tiene que tener al menos 8 carácteres.',
                'required' => 'Es un campo requerido.',
            ],
            'email' => [
                'min_length' => 'El email no es válido.',
                'valid_email' => 'El email no es válido',    
            ]
        ];

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $rules, $errors)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        return $this->getJWTForUser($input['email']);
    }

    private function getJWTForUser(
        string $emailAddress,
        int $responseCode = ResponseInterface::HTTP_OK
    )
    {
        try {
            $model = new UserModel();
            $user = $model->findUserByEmailAddress($emailAddress);
            unset($user['password']);

            helper('jwt');

            return $this
                ->getResponse(
                    [
                        'message' => 'Usuario autentificado',
                        'user' => $user,
                        'access_token' => getSignedJWTForUser($emailAddress)
                    ]
                );
        } catch (Exception $exception) {
            return $this
                ->getResponse(
                    [
                        'error' => $exception->getMessage(),
                    ],
                    $responseCode
                );
        }
    }
}
