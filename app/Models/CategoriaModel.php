<?php namespace App\Models;
use CodeIgniter\Model;

class CategoriaModel extends Model{
	protected $table = 'categoria';
	protected $primaryKey = 'id_categoria';
	protected $allowedFields = ['id_categoria','nombre','descripcion'];
	 
	/**
	* Obtenemos todas las categorias
	*/
	public function getAll(){
		$query = $this->query("SELECT * FROM categoria");
		return $query->getResult('array');
	}
	
	/**
	* Obtenemos una categoria 
	*/
	public function get($id){
		$sql = "SELECT * FROM categoria WHERE id_categoria=:id:";
		$query = $this->query($sql, [
		'id' => $id,
		]);
		return $query->getResult('array');
	}


	
}
