<?php namespace App\Models;
use CodeIgniter\Model;

class FavoritosModel extends Model{
	protected $table = 'favoritos';
	protected $primaryKey = 'id';
	protected $allowedFields = ['id','userid','productoid','imagenproducto','nombreproducto'];
	 
	/**
	* Obtenemos todos los productos favoritos 
	*/
	public function getAll(){
		$query = $this->query("SELECT * from favoritos");
		return $query->getResult('array');
	}
	
	/**
	* Obtenemos todos los productos favoritos por usuario
	*/
	public function get($id){
		$sql = "SELECT favoritos.*, productos.nombre AS nombreproducto, productos.imagen AS imagenproducto FROM favoritos INNER JOIN productos ON favoritos.productoid = productos.id WHERE userid=:id:";
		$query = $this->query($sql, ['id' => $id,]);
		return $query->getResult('array');
	}


	
}
