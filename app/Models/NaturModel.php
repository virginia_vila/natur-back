<?php namespace App\Models;
use CodeIgniter\Model;

class NaturModel extends Model{
	protected $table = 'productos';
	protected $primaryKey = 'id';
	protected $allowedFields = ['id','nombre','imagen','descripcion','tipo_producto','id_categ'];
	 
	/**
	* Obtenemos todos los productos
	*/
	public function getAll(){
		$query = $this->query("SELECT producto.*, categ.nombre AS nombre_categ FROM `productos` AS producto INNER JOIN categoria AS categ ON producto.id_categ=categ.id_categoria ORDER BY producto.id ASC");
		return $query->getResult('array');
	}
	
	/**
	* Obtenemos un único producto
	*/
	public function get($id){
		$sql = "SELECT producto.*, categ.nombre AS nombre_categ FROM `productos` AS producto INNER JOIN categoria AS categ ON producto.id_categ=categ.id_categoria WHERE id=:id:";
		$query = $this->query($sql, [
		'id' => $id,
		]);
		return $query->getResult('array');
	}


	
}
