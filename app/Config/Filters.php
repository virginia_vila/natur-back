<?php namespace Config;

use CodeIgniter\Config\BaseConfig;
use App\Filters\JWTAuthenticationFilter;
use App\Filters\CorsFilter;

class Filters extends BaseConfig
{
	// Makes reading things below nicer,
	// and simpler to change out script that's used.
	public $aliases = [
		'csrf'     => \CodeIgniter\Filters\CSRF::class,
		'toolbar'  => \CodeIgniter\Filters\DebugToolbar::class,
		'honeypot' => \CodeIgniter\Filters\Honeypot::class,
		'corsfilter'     => CorsFilter::class,
        'auth' => JWTAuthenticationFilter::class
	];

	// Always applied before every request
	public $globals = [
		'before' => [
			'corsfilter'
		],

		'after'  => [
			'toolbar',
			'corsfilter'
		],
	];

	// Works on all of a particular HTTP method
	// (GET, POST, etc) as BEFORE filters only
	//     like: 'post' => ['CSRF', 'throttle'],
	public $methods = [];

	// List filter aliases and any before/after uri patterns
	// that they should run on, like:

		public $filters = [
        'auth' => [
            'before' => ['restnatur/add','restnatur/update/*','restnatur/delete/*',
			'user/*','categorias/add','categorias/update/*','categorias/delete/*', 'favoritos/*']
        ]];


		
}
