<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}


// Usuarios
$routes->get('user', 'User::index');
$routes->get('user/(:num)', 'User::show/$1');
$routes->get('user/(:any)', 'User::showByEmail/$1');
$routes->post('user/(:num)', 'User::update/$1');
$routes->delete('user/(:num)', 'User::destroy/$1');

// Productos
$routes->get('restnatur/productos', 'RESTNatur::index');
$routes->get('restnatur/productos/(:num)', 'RESTNatur::show/$1');
$routes->post('restnatur/add', 'RESTNatur::create');
$routes->put('restnatur/update/(:num)', 'RESTNatur::update/$1');
$routes->delete('restnatur/delete/(:num)', 'RESTNatur::delete/$1');

// Categorias
$routes->get('categorias', 'Categoria::index');
$routes->post('categorias/add', 'Categoria::create');
$routes->put('categorias/update/(:num)', 'Categoria::update/$1');
$routes->delete('categorias/delete/(:num)', 'Categoria::delete/$1');

// Favoritos
$routes->get('favoritos/(:num)', 'Favoritos::show/$1');
$routes->post('favoritos/add', 'Favoritos::create');
$routes->delete('favoritos/delete/(:num)', 'Favoritos::delete/$1');